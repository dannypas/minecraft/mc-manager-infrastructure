data "google_client_config" "client" {}

provider "google" {
  project = var.project_id
  region  = var.region
}

provider "google-beta" {
  project = var.project_id
  region  = var.region
}

terraform {
#  backend "gcs" {
#    bucket = "dannypas00-mc-manager-tf-state-bucket"
#  }
}
