locals {
  enabled_apis = toset([
    "artifactregistry",
    "cloudbuild",
  ])
}

resource "google_project_service" "enabled_apis" {
  for_each           = local.enabled_apis
  service            = "${each.key}.googleapis.com"
  disable_on_destroy = true
}
