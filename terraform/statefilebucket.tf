resource "google_storage_bucket" "tf_state" {
  name          = "${var.project_id}-tf-state-bucket"
  force_destroy = false
  location      = var.region
  storage_class = "STANDARD"

  uniform_bucket_level_access = true

  versioning {
    enabled = true
  }

  lifecycle {
    prevent_destroy = true
    // Ignore all changes to statefile bucket because tf should never ever ever try to recreate it
    ignore_changes = all
  }
}
