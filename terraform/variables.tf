variable "project_id" {
  description = "GCP project ID"
  type        = string
  default     = "dannypas00-mc-manager"
}

variable "region" {
  description = "GCP region"
  type        = string
  default     = "europe-west4"
}

variable "zone" {
  description = "GCP zone"
  type        = string
  default     = "europe-west4-a"
}
